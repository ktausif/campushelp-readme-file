# Campus_Help
### An android application for my college ###
***Home Page***

![ch5](/uploads/cc3f52da4aa73de042d4f255bb7304ab/ch5.png)

***Search for Professor's cabin in the college*** 

![ch4](/uploads/9b496f25c3a70e8243493f40d039c8ae/ch4.png)

***After clicking on a professor's name*** 

![ch1](/uploads/556e8cb1a6b4c36f258166e7457bb270/ch1.png)

***Maps page where you can get almost every needy thing around the college***

![ch3](/uploads/7e0becb2e7715ef52b2c4d434e952dcb/ch3.png)

***If you are not aware of the buildings in the college then you can take help from this app***

![ch](/uploads/ec79417d5031e01552b92eb3460792fe/ch.png)


### This is just a `README` file, the complete project is in private ###





